//  Modal

$(".btn-modal").fancybox({
    'padding'    : 20,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"><i class="fa fa-times"></i></a>'
    }
});